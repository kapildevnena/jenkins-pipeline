def buildApp() {
    echo 'building application'
    echo "building with version ${NEW_VERSION}"
}

def testApp() {
    echo 'testing application'
}

def deployApp() {
    echo 'deploying application'
}

return this
